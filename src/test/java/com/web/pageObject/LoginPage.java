package com.web.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	WebDriver driver;

	public LoginPage(WebDriver dr) {
		this.driver = dr;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.NAME, using = "uid")
	@CacheLookup
	public WebElement textUserName;
	@FindBy(how = How.NAME, using = "password")
	@CacheLookup
	public WebElement txtPassword;
	@FindBy(how = How.XPATH, using = "//*[@name=\"btnLogin\"]")
	@CacheLookup
	public WebElement loginButton;
	@FindBy(how = How.XPATH, using = "//*[@name=\"btnReset\"]")
	@CacheLookup
	public WebElement resetButton;
}
