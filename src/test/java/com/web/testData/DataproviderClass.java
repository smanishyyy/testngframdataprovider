package com.web.testData;

import org.testng.annotations.DataProvider;

public class DataproviderClass {
@DataProvider(name="SearchProvider")
public static Object[][] getDataFromDataprovider()
{
	return new Object[][] {{"Guru99", "India"}, { "mngr243065", "qEqurEj" }, { "mmmm", "1234" }, { "nnnnn", "143" },{ "sssss", "143" }};
	
}
}
