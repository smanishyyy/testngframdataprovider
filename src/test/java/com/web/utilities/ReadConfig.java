package com.web.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {
	
	
	private static ReadConfig configObj=null;
	public  Properties pro;
	private ReadConfig()
	{
		File src=new File("./Configuration/config.properties");
		try
		{
		FileInputStream str=new FileInputStream(src);	
		pro=new Properties();
		pro.load(str);
		}
		catch(Exception e)
		{
			System.out.println("Exception is :"+e.getMessage());
		}
		
	}
	
	public static ReadConfig getInstance()
	{
		if(configObj==null)
		{
			return	configObj=new ReadConfig();	
		}
		else
		{
			return configObj;
		}
	}
	public String getChromepath()
	{
		return System.getProperty("user.dir")+pro.getProperty("chromePath");
		
	}
	public String getUrl()
	{
		return pro.getProperty("browserUrl");
		
	}
	public String getLog4jath()
	{
		return System.getProperty("user.dir")+ pro.getProperty("log4jPath");
		
	}
	public String getUserName()
	{
		return pro.getProperty("userID");
		
	}
	public String getPassword()
	{
		return pro.getProperty("password");
		
	}
	

}
