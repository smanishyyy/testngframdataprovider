package com.web.utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XLUtils {
	public static FileInputStream fi;
	public static FileOutputStream fo;
	public static XSSFWorkbook wb;
	public static XSSFSheet ws;
	public static XSSFRow row;
	public static XSSFCell cell;

	public static int getRowCount(String xlFile, String xlSheet) throws IOException {
		fi = new FileInputStream(xlFile);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet(xlSheet);
		int rowCount = ws.getLastRowNum();
		wb.close();
		fi.close();
		return rowCount;
	}

	public static int getCellCount(String xlFile, String xlSheet, int rownum) throws IOException {
		fi = new FileInputStream(xlFile);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet(xlSheet);
		row = ws.getRow(rownum);
		int cellCount = row.getLastCellNum();
		wb.close();
		fi.close();
		return cellCount;
	}

	public static String getCellData(String xlFile, String xlSheet, int rownum, int colnum) throws IOException {
		fi = new FileInputStream(xlFile);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet(xlSheet);
		row = ws.getRow(rownum);
		cell = row.getCell(colnum);

		String data;
		try {
			DataFormatter formatter = new DataFormatter();
			String cellData = formatter.formatCellValue(cell);
			wb.close();
			fi.close();
			return cellData;
		} catch (Exception e) {
			data = "";
			wb.close();
			fi.close();
			return data;
		}

	}

	public static void setCellData(String xlFile, String xlSheet, int rownum, int colnum, String data)
			throws IOException {
		fi = new FileInputStream(xlFile);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet(xlSheet);
		row = ws.getRow(rownum);
		cell = row.createCell(colnum);
		cell.setCellValue(data);
		fo = new FileOutputStream(xlFile);
		wb.write(fo);
		wb.close();
		fi.close();
		fo.close();

	}

	public static String getTestCaseName(String testCaseName) {
		String value = testCaseName;
		try

		{
			System.out.println("Value : " + value);
			int posi = value.indexOf("@");
			System.out.println("Position : " + posi);
			value = value.substring(0, posi);
			System.out.println("Value : " + value);
			posi = value.lastIndexOf(".");
			System.out.println("Position : " + posi);
			value = value.substring(posi + 1);
			System.out.println("Value : " + value);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public static ArrayList<Integer> getTestCaseRow(String testCaseName, int colNum, String xlFile, String xlSheet)
		 {
		try
		{
		fi = new FileInputStream(xlFile);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet(xlSheet);
		int rowCount = ws.getLastRowNum();
		ArrayList<Integer> al=new ArrayList<Integer>();
		System.out.println("RowCount : " + rowCount);
		int i;
		for (i = 1; i <=rowCount; i++) {
			String testName = XLUtils.getCellData(xlFile, xlSheet, i, colNum);
			System.out.println("Ts: " + testName);
			if (testName.trim().equalsIgnoreCase(testCaseName.trim())) {
//				break;
				al.add(i);
			}
		}
		return al;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		
	}
}
