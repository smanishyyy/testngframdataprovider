package com.web.utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.web.testCases.BaseClass;
import com.web.testCases.TC08_AgnTsName;

public class ListenerTest extends BaseClass implements ITestListener {

	

	@Override
	public void onFinish(ITestContext tr) {
		System.out.println("Thread  Listener onFinish : " + Thread.currentThread().getId());
//		extent.flush();

	}

	@Override
	public void onStart(ITestContext tr) {
		System.out.println("Thread  Listener onStart : " + Thread.currentThread().getId());
		/*SimpleDateFormat dateFormat = new SimpleDateFormat("yyy_mm_dd_hh_mm_ss");

		String timeStamp = dateFormat.format(new Date());
		System.out.println("timeStamp : " + timeStamp);
		String repName = "TestReport_" + timeStamp + ".html";
		System.out.println(System.getProperty("user.dir") + "/test-output/" + repName);
		HtmlReporter = new ExtentHtmlReporter(new File(System.getProperty("user.dir") + "//test-output//" + repName));
		HtmlReporter.loadConfig(System.getProperty("user.dir") + "//extent-config.xml");
		extent = new ExtentReports();
		extent.attachReporter(HtmlReporter);
		extent.setSystemInfo("Host name", "Local");
		extent.setSystemInfo("Enviroment", "QA");
		extent.setSystemInfo("User", "Manish");
		HtmlReporter.config().setDocumentTitle("NewExtent");
		HtmlReporter.config().setReportName("Functional Testing");
		HtmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
*/
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult tr) {
		// TODO Auto-generated method stub
		System.out.println("Thread  Listener onTestFailedButWithinSuccessPercentage : " + Thread.currentThread().getId());

	}
	@Override
	public void onTestFailure(ITestResult tr) {
		System.out.println("Thread  Listener onTestFailure : " + Thread.currentThread().getId());

		logger.log(Status.FAIL, MarkupHelper.createLabel(tr.getName(), ExtentColor.RED));

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyy_mm_dd_hh_mm_ss");
		String timeStamp = dateFormat.format(new Date());
	TakesScreenshot tk = ((TakesScreenshot) driver);
		File src = tk.getScreenshotAs(OutputType.FILE);

		StringTokenizer st1 = new StringTokenizer(System.getProperty("user.dir"), "\\");
		LinkedList<String> ls = new LinkedList<String>();

		while (st1.hasMoreTokens()) {
			ls.add(st1.nextToken());
		}

		StringBuilder ScreenshotDir = new StringBuilder();

		for (int i = 0; i < ls.size() - 1; i++) {
			ScreenshotDir.append("\\" + ls.get(i));

		}

		File dst = new File(
				"\\" + ScreenshotDir + "\\" + "TestCase" + "\\screenshots" + tr.getName() + timeStamp + ".png");
		try {
			FileUtils.copyFile(src, dst);
			logger.addScreenCaptureFromPath(dst.toString());
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void onTestSkipped(ITestResult tr) {
		System.out.println("Thread  Listener onTestSkipped : " + Thread.currentThread().getId());
		/*logger = extent.createTest(tr.getName());
		logger.log(Status.SKIP, MarkupHelper.createLabel(tr.getName(), ExtentColor.ORANGE));
*/
	}

	@Override
	public void onTestStart(ITestResult tr) {
		System.out.println("Thread  Listener onTestStart : " + Thread.currentThread().getId());
	}

	@Override
	public void onTestSuccess(ITestResult tr) {
		System.out.println("Thread  Listener onTestSuccess : " + Thread.currentThread().getId());
		/*logger = extent.createTest(tr.getName());
		logger.log(Status.PASS, MarkupHelper.createLabel(tr.getName(), ExtentColor.GREEN));*/

	}


	

	

	

}
