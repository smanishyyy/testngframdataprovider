package com.web.utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.web.testCases.BaseClass;

public class ListnerAdapter extends TestListenerAdapter{

	public void onTestFailure(ITestResult result) {
		System.out.println("Test Failed");
		System.out.println("Test failed :"+Thread.currentThread().getId() + "diver:" + BaseClass.driver2);	
	if (result.getStatus() == ITestResult.FAILURE) {
		BaseClass.logger.log(Status.FAIL,  result.getThrowable());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyy_mm_dd_hh_mm_ss");
		String timeStamp = dateFormat.format(new Date());
		TakesScreenshot tk = ((TakesScreenshot) BaseClass.driver2);
		File src = tk.getScreenshotAs(OutputType.FILE);

		StringTokenizer st1 = new StringTokenizer(System.getProperty("user.dir"), "\\");
		LinkedList<String> ls = new LinkedList<String>();

		while (st1.hasMoreTokens()) {
			ls.add(st1.nextToken());
		}

		StringBuilder ScreenshotDir = new StringBuilder();

		for (int i = 0; i < ls.size() - 1; i++) {
			ScreenshotDir.append("\\" + ls.get(i));

		}

		File dst = new File("\\" + ScreenshotDir + "\\" + "TestCase" + "\\screenshots"
				+  result.getMethod().getMethodName() + timeStamp + ".png");
		try {
			FileUtils.copyFile(src, dst);
			BaseClass.logger.addScreenCaptureFromPath(dst.toString());
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	
	}

	
	public void onTestSkipped(ITestResult tr) {
		System.out.println("Test Skipped");	
	}


	public void onTestStart(ITestResult tr) {
		
		System.out.println("Test Start ");	

	}

	
	public void onTestSuccess(ITestResult tr) {
	
		System.out.println("Test Passed");
	}
	
	

}
