package com.web.utilities;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.annotations.DataProvider;

public class XlDataprovider {

	public static String[][] getXlDataProvider(String path, String sheetName) throws IOException {
		int rownum = XLUtils.getRowCount(path, sheetName);
		int colnum = XLUtils.getCellCount(path, sheetName, 1);
		System.out.println("Row : " + rownum);
		System.out.println("Column : " + colnum);
		String[][] data = new String[rownum][colnum];
		for (int i = 1; i <= rownum; i++) {
			for (int j = 0; j < colnum; j++) {
				data[i - 1][j] = XLUtils.getCellData(path, sheetName, i, j);
			}
		}
		return data;
	}

	public static String[][] getXlDataProviderForTestCase(String path, String sheetName, int rowNum) {
		String data2[][] = null;
		try {
			int colnum = XLUtils.getCellCount(path, sheetName, 1);
			System.out.println("Row : " + rowNum);
			System.out.println("Column : " + colnum);
			String[][] data = new String[rowNum][colnum];
			data2 = new String[1][colnum];

			for (int i = rowNum; i <= rowNum; i++) {
				for (int j = 0; j <= colnum; j++) {
					data2[0][j] = XLUtils.getCellData(path, sheetName, rowNum, j);
					System.out.println(data2[0][j]);

				}
			}
			return data2;
		} catch (Exception e) {
			/* e.printStackTrace(); */
			return data2;
		}
	}

	public static String[][] getXlDataProviderForTestCase2(String path, String sheetName,
			ArrayList<Integer> iTestCaseRow) {

		String data2[][] = null;
		try {
			int colnum = XLUtils.getCellCount(path, sheetName, 1);
			int row2 = XLUtils.getRowCount(path, sheetName);
			System.out.println("Row2 : " + row2);
			int rowNum = iTestCaseRow.size();
			System.out.println("Row : " + rowNum);

			System.out.println("Column : " + colnum);

			data2 = new String[rowNum][colnum];

			for (int i = 0; i < rowNum; i++) {
				for (int j = 0; j < colnum; j++) {
					data2[i][j] = XLUtils.getCellData(path, sheetName, iTestCaseRow.get(i), j);
					System.out.println(data2[i][j]);

				}
			}
			return data2;
		} catch (Exception e) {
			/* e.printStackTrace(); */
			return data2;
		}
	}
}
