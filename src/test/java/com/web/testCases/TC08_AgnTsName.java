package com.web.testCases;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.web.pageObject.LoginPage;

import com.web.utilities.XLUtils;
import com.web.utilities.XlDataprovider;

public class TC08_AgnTsName extends BaseClass {
	String sheetName, path, testCaseName;
	ArrayList<Integer> iTestCaseRow;

	@Test(dataProvider = "dataProvider", groups = "A")
	public void loginTest08(String testCaseName, String userName, String password)
			throws InterruptedException {
		try {
			System.out.println("Extentttttt:"+extent);
			log = Logger.getLogger(TC08_AgnTsName.class);
			logger = extent.createTest("loginTest08" + "__" + userName);
			logger.info("Launch url");
			driver.get(super.url);

			log.info("Application launched successfully");
			LoginPage lg = new LoginPage(driver);
			lg.textUserName.sendKeys(userName);
			logger.info("User enter username as " + userName);

			lg.txtPassword.sendKeys(password);
			logger.info("User enter password as " + password);

			lg.loginButton.click();
			logger.info("User click on login buttonn ");

			System.out.println(driver.getTitle());
			if (driver.getTitle().equals("Guru99 Bank Manager HomePage")) {
				Assert.assertTrue(true);
				log.debug("Here is some DEBUG");
				log.info("Here is some INFO");
				log.warn("Here is some WARN");
				log.error("Here is some ERROR");
				log.fatal("Here is some FATAL");
				logger.log(Status.PASS, "Assert Pass condition is true");
			} else {
				Assert.assertTrue(false);
			}

		} catch (Exception e) {

			Alert al = driver.switchTo().alert();
			al.accept();

			Assert.assertTrue(true);
			logger.log(Status.FAIL, "Assert Fai condition is false");
		}
	}

	@DataProvider(name = "dataProvider")
	public String[][] getDataProvider() throws IOException {
		path = System.getProperty("user.dir") + "//src//test//java//com//web//testData//TestData.xlsx";
		sheetName = "Sheet2";
		testCaseName = this.toString();
		testCaseName = XLUtils.getTestCaseName(testCaseName);
		System.out.println("TestCase Name  : " + testCaseName);
		String xlFile = path;
		String xlSheet = "Sheet2";
		ArrayList<Integer> iTestCaseRow = XLUtils.getTestCaseRow(testCaseName, 0, xlFile, xlSheet);
		System.out.println(iTestCaseRow);
		String data[][] = XlDataprovider.getXlDataProviderForTestCase2(path, sheetName, iTestCaseRow);

		return data;

	}
}