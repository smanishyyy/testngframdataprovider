package com.web.testCases;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.web.pageObject.LoginPage;
@Test
public class TC03_LoginTest extends BaseClass {

	@Test(dataProvider = "SearchProvider",groups= {"TC03_LoginTest"})
	public void loginTest3(ITestContext tr, String userName, String password) throws InterruptedException {
		System.out.println("Thread id TC03_LoginTest test method : "+Thread.currentThread().getId());
		try {
			System.out.println("Extentttttt:"+extent);
			System.out.println("Driver  of loginTest  "+driver);
			log = Logger.getLogger(TC03_LoginTest.class);
		logger = extent.createTest("loginTest3" + "__" + userName);
			driver.get(super.url);

			log.info("Application launched successfully");
			LoginPage lg = new LoginPage(driver);
			lg.textUserName.sendKeys(userName);
			log.info(userName);
			lg.txtPassword.sendKeys(password);
			log.info(password);
			lg.loginButton.click();

			System.out.println(driver.getTitle());
			if (driver.getTitle().equals("Guru99 Bank Manager HomePage")) {
				Assert.assertTrue(true);
				log.debug("Here is some DEBUG");
				log.info("Here is some INFO");
				log.warn("Here is some WARN");
				log.error("Here is some ERROR");
				log.fatal("Here is some FATAL");
			} else {
				Assert.assertTrue(false);
			}
		} catch (Exception e) {
			Alert al = driver.switchTo().alert();
			al.accept();
			Assert.assertTrue(false);
			logger.log(Status.FAIL, "Assert Fai condition is false");
		}
	}

}
