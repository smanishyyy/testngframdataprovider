package com.web.testCases;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.web.pageObject.LoginPage;
import com.web.utilities.XlDataprovider;

public class TC07_LoginTest extends BaseClass {
	@Test(dataProvider = "dataProvider", groups = "A")
	public void loginTest07(ITestContext tr, String userName, String password) throws InterruptedException {

		try {
			log = Logger.getLogger(TC07_LoginTest.class);

			driver.get(super.url);

			log.info("Application launched successfully");
			LoginPage lg = new LoginPage(driver);
			lg.textUserName.sendKeys(userName);

			lg.txtPassword.sendKeys(password);

			lg.loginButton.click();

			System.out.println(driver.getTitle());
			if (driver.getTitle().equals("Guru99 Bank Manager HomePage")) {
				Assert.assertTrue(true);
				log.debug("Here is some DEBUG");
				log.info("Here is some INFO");
				log.warn("Here is some WARN");
				log.error("Here is some ERROR");
				log.fatal("Here is some FATAL");
			} else {
				Assert.assertTrue(false);
			}
		} catch (Exception e) {
			Alert al = driver.switchTo().alert();
			al.accept();
		}
	}
	
	@DataProvider(name="dataProvider")
	public static String[][] getDataProvider() throws IOException
	{
		String path=System.getProperty("user.dir")+"//src//test//java//com//web//testData//TestData.xlsx";
		String sheetName="Sheet1";
		System.out.println("Data :"+XlDataprovider.getXlDataProvider(path,sheetName));
		return XlDataprovider.getXlDataProvider(path,sheetName);
		
	}
}
