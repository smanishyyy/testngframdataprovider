package com.web.testCases;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.web.pageObject.LoginPage;
import com.web.testData.DataproviderClass;
@Test
public class TC04_LoginTest  extends BaseClass{
	@Test(dataProvider = "SearchProvider", dataProviderClass=DataproviderClass.class,groups= {"TC04_LoginTest"})
	public void loginTest4( String userName, String password) throws InterruptedException {

		try {
			log = Logger.getLogger(TC04_LoginTest.class);
			logger = extent.createTest("loginTest4" + "__" + userName);
			driver.get(super.url);

			log.info("Application launched successfully");
			LoginPage lg = new LoginPage(driver);
			lg.textUserName.sendKeys(userName);
			logger.info("User enter username as " + userName);
			lg.txtPassword.sendKeys(password);
			logger.info("User enter password as " + password);
			lg.loginButton.click();

			System.out.println(driver.getTitle());
			if (driver.getTitle().equals("Guru99 Bank Manager HomePage")) {
				Assert.assertTrue(true);
				log.debug("Here is some DEBUG");
				log.info("Here is some INFO");
				log.warn("Here is some WARN");
				log.error("Here is some ERROR");
				log.fatal("Here is some FATAL");
			} else {
				Assert.assertTrue(false);
			}
		} catch (Exception e) {
			Alert al = driver.switchTo().alert();
			al.accept();
			Assert.assertTrue(false);
			logger.log(Status.FAIL, "Assert Fai condition is false");
		}
	}

}
