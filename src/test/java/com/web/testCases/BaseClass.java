package com.web.testCases;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.web.utilities.ReadConfig;

public class BaseClass {

	public String url = ReadConfig.getInstance().getUrl();
	public String userName = ReadConfig.getInstance().getUserName();
	public String password = ReadConfig.getInstance().getPassword();
	public   WebDriver driver;
	public  static WebDriver driver2;
	public static Logger log;
	public static ExtentHtmlReporter HtmlReporter;
	public static ExtentReports extent;
	public static ExtentTest logger;

	
	
	@BeforeClass(groups = { "A", "B", "TC02_LoginTest" })
	@Parameters("browserName")
	public void setUp(String br) {
		if (br.equals("chrome")) {
			driver = new ChromeDriver();
			System.setProperty("webdriver.chrome.driver", ReadConfig.getInstance().getChromepath());
			System.out.println("Thread id beforemethod method : " + Thread.currentThread().getId() + "diver:" + driver);
		}

		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver2=driver;
		// PropertyConfigurator.configure(ReadConfig.getInstance().getLog4jath());
		// DOMConfigurator.configure(ReadConfig.getInstance().getLog4jath());

		log = Logger.getLogger(BaseClass.class);
		/* ListenerTest.logger.info("Launch browser"); */
		log.debug("Here is some DEBUG");
		log.info("Here is some INFO");
		log.warn("Here is some WARN");
		log.error("Here is some ERROR");
		log.fatal("Here is some FATAL");

	}

	@BeforeTest
	public void startReport() {
		System.out.println("Thread id beforetest method : " + Thread.currentThread().getId());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyy_mm_dd_hh_mm_ss");

		String timeStamp = dateFormat.format(new Date());
		System.out.println("timeStamp : " + timeStamp);
		String repName = "TestReport_" + timeStamp + ".html";
		System.out.println(System.getProperty("user.dir") + "/test-output/" + repName);
		HtmlReporter = new ExtentHtmlReporter(new File(System.getProperty("user.dir") + "//test-output//" + repName));
		HtmlReporter.loadConfig(System.getProperty("user.dir") + "//extent-config.xml");
		extent = new ExtentReports();
		extent.attachReporter(HtmlReporter);
		extent.setSystemInfo("Host name", "Local");
		extent.setSystemInfo("Enviroment", "QA");
		extent.setSystemInfo("User", "Manish");
		HtmlReporter.config().setDocumentTitle("NewExtent");
		HtmlReporter.config().setReportName("Functional Testing");
		HtmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
	}

	@DataProvider(name = "SearchProvider")
	public Object[][] getDataFromDataprovider() {
		return new Object[][] { { "mngr243065", "143" }
				// , { "mmmm", "1234" }, { "nnnnn", "qEqurEj" }
				,
				// { "sssss", "143" }
		};
	}

	@AfterClass(groups = { "A", "B" })
	public void tearDown(ITestContext result) {
		System.out.println("Thread id @AfterClass method : " + Thread.currentThread().getId());
		
		
		
	}

	@AfterTest
	public void endReport() {
		System.out.println("Thread id AfterTest method : " + Thread.currentThread().getId());
		
		driver.quit();
		extent.flush();
		
	}
}
