package com.web.testCases;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.web.pageObject.LoginPage;

public class TC06_LoginTest extends BaseClass {
	@Test(dataProvider = "SearchProvider2",groups="A")
	public void loginTest06(ITestContext tr, String userName, String password) throws InterruptedException {

		try {
			log = Logger.getLogger(TC06_LoginTest.class);

			driver.get(super.url);

			log.info("Application launched successfully");
			LoginPage lg = new LoginPage(driver);
			lg.textUserName.sendKeys(userName);

			lg.txtPassword.sendKeys(password);

			lg.loginButton.click();

			System.out.println(driver.getTitle());
			if (driver.getTitle().equals("Guru99 Bank Manager HomePage")) {
				Assert.assertTrue(true);
				log.debug("Here is some DEBUG");
				log.info("Here is some INFO");
				log.warn("Here is some WARN");
				log.error("Here is some ERROR");
				log.fatal("Here is some FATAL");
			} else {
				Assert.assertTrue(false);
			}
		} catch (Exception e) {
			Alert al = driver.switchTo().alert();
			al.accept();
		}
	}
	
	@Test(dataProvider = "SearchProvider2",groups="B")
	public void loginTest6(ITestContext tr, String userName, String password) throws InterruptedException {

		try {
			log = Logger.getLogger(TC06_LoginTest.class);

			driver.get(super.url);

			log.info("Application launched successfully");
			LoginPage lg = new LoginPage(driver);
			lg.textUserName.sendKeys(userName);

			lg.txtPassword.sendKeys(password);

			lg.loginButton.click();

			System.out.println(driver.getTitle());
			if (driver.getTitle().equals("Guru99 Bank Manager HomePage")) {
				Assert.assertTrue(true);
				log.debug("Here is some DEBUG");
				log.info("Here is some INFO");
				log.warn("Here is some WARN");
				log.error("Here is some ERROR");
				log.fatal("Here is some FATAL");
			} else {
				Assert.assertTrue(false);
			}
		} catch (Exception e) {
			Alert al = driver.switchTo().alert();
			al.accept();
		}
	}
	@DataProvider(name="SearchProvider2")
	public Object[][] getDataFromDataprovider(ITestContext c){
	Object[][] groupArray = null;
		for (String group : c.getIncludedGroups()) {
		if(group.equalsIgnoreCase("A")){
			groupArray = new Object[][] { 
					{ "Guru99", "India" }, 
					{ "Krishna", "UK" }, 
					{ "Bhupesh", "USA" } 
				};
		break;	
		}
			else if(group.equalsIgnoreCase("B"))
			{
			groupArray = new Object[][] { 
						{  "Canada","Guru99", }, 
						{  "Russia" ,"Krishna",}, 
						{  "Japan", "Bhupesh", } 
					};
			}
		break;
	}
	return groupArray;		
	}
	
}
