package com.web.testCases;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.cucumber.listener.Reporter;
import com.web.pageObject.LoginPage;


public class TC01_LoginTest extends BaseClass {

	@Test(priority = 3, groups = { "TC01_LoginTest" })
	public void loginTest(ITestContext tr) {
		try
		{
		log = Logger.getLogger(TC01_LoginTest.class);
logger = extent.createTest("loginTest1" + "__" + userName);
		System.out.println("Extentttttt:"+extent);
		System.out.println("Thread  test TC01_LoginTest : " + Thread.currentThread().getId());
		System.out.println("Driver  of loginTest  "+driver);
		driver.get(super.url);

		log.info("Application launched successfully");
		LoginPage lg = new LoginPage(driver);
		lg.textUserName.sendKeys(super.userName);

		lg.txtPassword.sendKeys(super.password);

		lg.loginButton.click();

		System.out.println(driver.getTitle());
		if (driver.getTitle().trim().equals("Guru99 Bank Manager HomePage")) {
			Assert.assertTrue(true);
			log.debug("Here is some DEBUG");
			log.info("Here is some INFO");
			log.warn("Here is some WARN");
			log.error("Here is some ERROR");
			log.fatal("Here is some FATAL");
		} else {
			Assert.assertTrue(false);
		}
		}
		catch (Exception e) {
			Alert al = driver.switchTo().alert();
			al.accept();
			Assert.assertTrue(false);
			logger.log(Status.FAIL, "Assert Fai condition is false");
		}
	}

	/*@Test(priority = 2)
	public void zz() {
		System.out.println("@Test zz");
	}

	@Test(dependsOnGroups = { "depend" })
	public void tb() {
		System.out.println("@tbt tb tbtb");
		Assert.assertEquals(true, true);
	}

	@Test(priority = 1)
	public void t1() {
		System.out.println("@Test ddd1");
	}

	@Test(priority = 0, groups = { "sanity", "smoke", "regression" }, invocationCount = 10, enabled = true)
	@Parameters("browserName")
	public void aa(@Optional String str) {
		System.out.println("@Test 11");
		System.out.println(str);

	}

	@Test(dependsOnMethods = "tb")
	public void next1() {
		System.out.println("next1 next1 next1");
	}

	@Test(groups = { "depend" })
	public void excludeGroup() {
		System.out.println("@dependddddd");
		Assert.assertEquals(true, true);
	}*/

}
