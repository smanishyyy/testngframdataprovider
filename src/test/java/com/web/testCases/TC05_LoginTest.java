package com.web.testCases;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.web.pageObject.LoginPage;


public class TC05_LoginTest extends BaseClass{
	@Test(dataProvider = "SearchProvider2")
	public void loginTest5(ITestContext tr, String userName, String password) throws InterruptedException {

		try {
		log = Logger.getLogger(TC05_LoginTest.class);

			driver.get(super.url);

			log.info("Application launched successfully"+Thread.currentThread().getId());
			LoginPage lg = new LoginPage(driver);
			lg.textUserName.sendKeys(userName);

			lg.txtPassword.sendKeys(password);

			lg.loginButton.click();

			System.out.println(driver.getTitle());
			if (driver.getTitle().equals("Guru99 Bank Manager HomePage")) {
				Assert.assertTrue(true);
				log.debug("Here is some DEBUG");
				log.info("Here is some INFO");
				log.warn("Here is some WARN");
				log.error("Here is some ERROR");
				log.fatal("Here is some FATAL");
			} else {
				Assert.assertTrue(false);
			}
		} catch (Exception e) {
			Alert al = driver.switchTo().alert();
			al.accept();
		}
	}
	
	/*@Test(dataProvider = "SearchProvider2")
	public void loginTest05(ITestContext tr, String userName, String password) throws InterruptedException {

		try {
			log = Logger.getLogger(TC05_LoginTest.class);

			driver.get(super.url);

			log.info("Application launched successfully"+Thread.currentThread().getId());
			LoginPage lg = new LoginPage(driver);
			lg.textUserName.sendKeys(userName);

			lg.txtPassword.sendKeys(password);

			lg.loginButton.click();

			System.out.println(BaseClass.driver.getTitle());
			if (BaseClass.driver.getTitle().equals("Guru99 Bank Manager HomePage")) {
				Assert.assertTrue(true);
				log.debug("Here is some DEBUG");
				log.info("Here is some INFO");
				log.warn("Here is some WARN");
				log.error("Here is some ERROR");
				log.fatal("Here is some FATAL");
			} else {
				Assert.assertTrue(false);
			}
		} catch (Exception e) {
			Alert al = driver.switchTo().alert();
			al.accept();
		}
	}*/
	
	@DataProvider(name = "SearchProvider2")
	public Object[][] getDataFromDataprovider(Method m) {
		if(m.getName().equalsIgnoreCase("loginTest5"))
		{
		return new Object[][] { { "mngr243065", "qEqurEj" } };
		}
		else
		{
			return new Object[][] { { "cvb", "1234" }, { "dfgh", "1234" }, { "sfdn", "143" },{ "bvcb", "143" } };	
		}
	}
}
