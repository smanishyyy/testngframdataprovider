package com.web.testCases;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.pageObject.LoginPage;


public class TC02_LoginTest extends BaseClass {

	@Parameters({ "userName", "password" })
	@Test(groups = { "TC02_LoginTest" })
	public void loginTest2(@Optional("mngr243065") String userName, @Optional String password) {
		System.out.println("Thread id  test 2 : "+Thread.currentThread().getId());
		System.out.println("Extentttttt:"+extent);
		log = Logger.getLogger(TC02_LoginTest.class);
		System.out.println("Extentttttt:"+extent);
	logger = extent.createTest("loginTest2" + "__" + userName);
		driver.get(super.url);

		log.info("Application launched successfully");
		LoginPage lg = new LoginPage(driver);
		lg.textUserName.sendKeys(userName);
	logger.info("User enter username as " + userName);
		lg.txtPassword.sendKeys(password);
	logger.info("User enter password as " + password);
		lg.loginButton.click();

		if (driver.getTitle().equals("Guru99 Bank Manager HomePage")) {

			Assert.assertTrue(false);
			log.debug("Here is some DEBUG");
			log.info("INFO");
			log.warn("WARN");
			log.error("ERROR");
			log.fatal("FATAL");
		} else {

			Assert.assertTrue(false);
		}

	}
}
